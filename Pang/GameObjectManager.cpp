#include "stdafx.h"
#include "GameObjectManager.h"



//Constructor
GameObjectManager::GameObjectManager()
{	
}
//Destructor
GameObjectManager::~GameObjectManager()
{
	//go from beginning to end using iterator, and run GameObjectDeallocator() on each
	std::for_each(_gameObjects.begin(),_gameObjects.end(),GameObjectDeallocator());
}
//add a new VGO to the manager
void GameObjectManager::add(std::string name, VisibleGameObject* gameObject)
{
	_gameObjects.insert(std::pair<std::string,VisibleGameObject*>(name,gameObject));
}

//remove a named VGO from manager
void GameObjectManager::remove(std::string name)
{
	//get an iterator that is the result of searching for the name given
	std::map<std::string, VisibleGameObject*>::iterator results = _gameObjects.find(name);
	//if it is not the end, release
	if(results != _gameObjects.end() )
	{
		delete results->second;
		_gameObjects.erase(results);
	}
}

//returns the given VisibleGameObject reference
VisibleGameObject* GameObjectManager::get(std::string name) const
{
	std::map<std::string, VisibleGameObject*>::const_iterator results = _gameObjects.find(name);
	if(results == _gameObjects.end() )
		return NULL;
	return results->second;
	
}

int GameObjectManager::getObjectCount() const
{
	return _gameObjects.size();
}


void GameObjectManager::drawAll(sf::RenderWindow& renderWindow)
{
	std::map<std::string,VisibleGameObject*>::const_iterator itr = _gameObjects.begin();
	while(itr != _gameObjects.end())
	{
		itr->second->draw(renderWindow);
		itr++;
	}
}