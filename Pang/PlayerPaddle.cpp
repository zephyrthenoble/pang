#include "StdAfx.h"
#include "PlayerPaddle.h"
#include "Game.h"


PlayerPaddle::PlayerPaddle() :
_velocity(0),
	_maxVelocity(600.0f)
{
	load("images/paddle.png");
	assert(isLoaded());
	sf::Sprite loaded = getSprite();
	const sf::Texture* tx = loaded.getTexture();
	loaded.setOrigin((float)tx->getSize().x/2,(float)tx->getSize().y/2);

}

PlayerPaddle::~PlayerPaddle()
{
}

void PlayerPaddle::draw(sf::RenderWindow& rw)
{
	VisibleGameObject::draw(rw);
}
float PlayerPaddle::getVelocity() const
{
	return _velocity;
}
void PlayerPaddle::update(float elapsedTime)
{
	if(Game::getInput().isKeyPressed(sf::Keyboard::Left))
    {
        _velocity-= 3.0f;
    }
    if(Game::getInput().isKeyPressed(sf::Keyboard::Right))
    {
        _velocity+= 3.0f;
    }

    if(Game::getInput().IsKeyDown(sf::Key::Down))
    {
        _velocity= 0.0f;
    }

    if(_velocity > _maxVelocity)
        _velocity = _maxVelocity;

    if(_velocity < -_maxVelocity)
        _velocity = -_maxVelocity;


    sf::Vector2f pos = this->getPosition();

    if(pos.x  < getSprite().getTexture()->getSize().x/2
        || pos.x > (Game::SCREEN_WIDTH - getSprite().getTexture()->getSize().x/2))
    {
        _velocity = -_velocity; // Bounce by current velocity in opposite direction
    }
    
    getSprite().move(_velocity * elapsedTime, 0);
}
