#include "stdafx.h"
#include "MainMenu.h"

#include <iostream>
using namespace std;

MainMenu::MenuResult MainMenu::Show(sf::RenderWindow& window)
{

	//Load menu image from file
	sf::Texture texture;
	texture.loadFromFile("images/mainmenu.png");
	sf::Sprite sprite(texture);

	//Setup clickable regions

	//Play menu item coordinates
	MenuItem playButton;
	sf::IntRect r1(0, 200, 1024, 200);
	playButton.rect = r1;
	playButton.action = Play;

	MenuItem exitButton;
	sf::IntRect r2(0, 400, 1024, 200);
	exitButton.rect = r2;
	exitButton.action = Exit;

	_menuItems.push_back(playButton);
	_menuItems.push_back(exitButton);

	window.draw(sprite);
	window.display();

	return GetMenuResponse(window);
}

MainMenu::MenuResult MainMenu::HandleClick(int x, int y)
{
	std::list<MenuItem>::iterator it;
//cout<<"click is "<<x<<" "<<y<<endl;
	int xpos = 0;
	for ( it = _menuItems.begin(); it != _menuItems.end(); it++)
	{
		xpos++;
		//cout << xpos << endl;
		sf::Rect<int> menuItemRect = (*it).rect;
		if( menuItemRect.contains(x, y))
			{
				//cout<<"Print" <<endl;
				return (*it).action;
			}
	}

	return Nothing;
}

MainMenu::MenuResult  MainMenu::GetMenuResponse(sf::RenderWindow& window)
{
	sf::Event menuEvent;

	while(true)
	{

		while(window.pollEvent(menuEvent))
		{
			if(menuEvent.type == sf::Event::MouseButtonPressed)
			{
				return HandleClick(menuEvent.mouseButton.x, menuEvent.mouseButton.y);
			}
			if(menuEvent.type == sf::Event::Closed)
			{
				return Exit;
			}
		}
	}
}